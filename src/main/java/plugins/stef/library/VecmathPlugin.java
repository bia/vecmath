package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Vecmath library (1.6.0) for Icy
 * 
 * @author Stephane Dallongeville
 */
public class VecmathPlugin extends Plugin implements PluginLibrary
{
    //
}
